/* 
Einstein's riddle
The situation
There are 5 houses in five different colors.
In each house lives a person with a different nationality.
These five owners drink a certain type of beverage, smoke a certain brand of cigar and keep a certain pet.
No owners have the same pet, smoke the same brand of cigar or drink the same beverage.
The question is: Who owns the fish?
 
Hints
#01 the Brit lives in the red house
#02 the Swede keeps dogs as pets
#03 the Dane drinks tea
#04 the green house is on the left of the white house
#05 the green house's owner drinks coffee
#06 the person who smokes Pall Mall rears birds
#07 the owner of the yellow house smokes Dunhill
#08 the man living in the center house drinks milk
#09 the Norwegian lives in the first house
#10 the owner who smokes BlueMaster drinks beer
#11 the German smokes Prince
#12 the Norwegian lives next to the blue house
#13 the man who smokes blend has a neighbor who drinks water
#14 the man who smokes blends lives next to the one who keeps cats
#15 the man who keeps horses lives next to the man who smokes Dunhill
 
Einstein wrote this riddle this century. He said that 98% of the world could not solve it.  
*/

const options = [
    ["red", "green", "white", "yellow", "blue"], //colors
    ["Brit", "Swede", "Dane", "Norwegian", "German"], //nationality
    ["tea", "coffee", "milk", "beer", "water"], //beverage
    ["Pall Mall", "Dunhill", "blends", "Prince", "BlueMaster"], //cigar
    ["dog", "birds", "cats", "horses", "fish"] //pet
];


function check_if_valid(houses) {

    for (let i = 0, l = houses.length; i < l; i++) {
        let  //colors,nationality,beverage,cigar,pet
            [c, n, b, g, p] = houses[i], // current house vals
            [cp, np, bp, gp, pp] = (i > 0) ? houses[i - 1] : [], //prevs house vals
            [cn, nn, bn, gn, pn] = (i + 1 < l) ? houses[i + 1] : [], //next house vals
            isN = (i + 1 < l || i == 4) // only if next exist or last solution (The 5th)

        if (
            /*#01*/ (n == "Brit" ^ c == "red") ||
            /*#02*/ (n == "Swede" ^ p == "dog") ||
            /*#03*/ (n == "Dane" ^ b == "tea") ||
            /*#04*/ (cp == "green" ^ c == "white") ||
            /*#05*/ (c == "green" ^ b == "coffee") ||
            /*#06*/ (g == "Pall Mall" ^ p == "birds") ||
            /*#07*/ (c == "yellow" ^ g == "Dunhill") ||
            /*#08*/ (i == 2 && b != "milk") ||
            /*#09*/ (i == 0 && n != "Norwegian") ||
            /*#10*/ (g == "BlueMaster" ^ b == "beer") ||
            /*#11*/ (n == "German" ^ g == "Prince") ||
            /*#12*/ (isN && n == "Norwegian" && cp != "blue" && cn != "blue") ||
            /*#13*/ (isN && g == "blends" && bp != "water" && bn != "water") ||
            /*#14*/ (isN && g == "blends" && pp != "cats" && pn != "cats") ||
            /*#15*/ (isN && p == "horses" && gp != "Dunhill" && gn != "Dunhill")
        ) return false;
    }
    return true //all tests passed;
}


function find_house_combo(houses = []) {

    let remainings = options.map(arr => [...arr]); //deep clone all options values

    //remove used options from each house combo to find the remaining options
    houses.forEach(house => house.forEach((val, i) =>
        remainings[i].splice(remainings[i].indexOf(val), 1)
    ));

    remainings.forEach(arr => { arr.l = arr.length; arr.i = 0 }); //shortcut arr.l , add index i

    let [c, n, b, g, p] = remainings,
        options_len = c.l * n.l * b.l * g.l * p.l;

    for (let i = 0; i < options_len; i++ , c.i++) {

        //increment index for each attr, so that we get all combo 
        for (let val_i = 0; val_i < remainings.length; val_i++)
            if (remainings[val_i].i == remainings[val_i].l) {
                remainings[val_i].i = 0; remainings[val_i + 1].i++;
            }

        //add the new house combo to houses array
        let new_houses = [...houses, remainings.map(arr => arr[arr.i])];

        //discard if not valid combo
        if (!check_if_valid(new_houses)) continue;

        if (new_houses.length < 5)
            find_house_combo(new_houses); //find the next house combo
        else
            console.log(JSON.stringify(new_houses)); //log the solution
    }

}


find_house_combo();